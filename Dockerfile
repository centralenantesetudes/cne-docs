FROM python:3.6-stretch
RUN apt-get -qq update && \
    apt-get install -y locales texlive texlive-base texlive-xetex texlive-latex-extra texlive-lang-french latexmk
# RUN sed -i 's/main/main\ contrib\ non\-free/g' /etc/apt/sources.list
RUN pip install pyyaml jinja2
RUN echo 'fr_FR.UTF-8 UTF-8' >> /etc/locale.gen
RUN locale-gen 
ENV LC_ALL=fr_FR.UTF-8
ENV LC_CTYPE=fr_FR.UTF-8
ENV USER=user
RUN mkdir -p /usr/share/fonts/truetype/century && \
	wget -q -O /usr/share/fonts/truetype/century/CenturyGothic.ttf http://www.911fonts.com/fonts/font-download-file/42933 && \
	wget -q -O /usr/share/fonts/truetype/century/CenturyGothicRegular.ttf http://www.911fonts.com/fonts/font-download-file/4365 && \
	wget -q -O /usr/share/fonts/truetype/century/CenturyGothicBold.ttf http://www.911fonts.com/fonts/font-download-file/4637 && \
	wget -q -O /usr/share/fonts/truetype/century/CenturyGothicBoldItalic.ttf http://www.911fonts.com/fonts/font-download-file/4638 && \
	wget -q -O /usr/share/fonts/truetype/century/CenturyGothicItalic.ttf http://www.911fonts.com/fonts/font-download-file/4641 && \
	chmod -R 644 /usr/share/fonts/truetype/century
RUN fc-cache -f
RUN mkdir -p /opt/cne/dist
COPY ./templates/ /opt/cne/templates
COPY ./generate.py /opt/cne
VOLUME /opt/cne/dist
VOLUME /opt/cne/src
WORKDIR /opt/cne
ENTRYPOINT ["python", "generate.py"]
CMD ["-h"]