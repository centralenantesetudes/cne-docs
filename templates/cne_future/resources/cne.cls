% Template CNE
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cne}

\LoadClass[11pt]{article}
\RequirePackage[margin=2.2cm]{geometry}

% Dépendances
\RequirePackage{textcomp}
\RequirePackage{fancyhdr}
\RequirePackage{lastpage}
\RequirePackage{graphicx}
\RequirePackage{tabularx}
\RequirePackage{color}
\RequirePackage[table]{xcolor}
\RequirePackage{colortbl}
\RequirePackage{sectsty}
\RequirePackage{makecell}
\RequirePackage{wrapfig}
\RequirePackage{hyperref}
\RequirePackage{tikz}
\RequirePackage[loadonly,nobottomtitles]{titlesec}
\RequirePackage{titletoc}

% Police
% nécesite XeLaTeX
\RequirePackage{fontspec}
\defaultfontfeatures{Ligatures=TeX}
\setmainfont{Century Gothic}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Généralités
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Couleurs de CNE
\definecolor{cne_blue}{HTML}{0070C0}
\definecolor{cne_lightblue}{HTML}{DAEEF3}
\definecolor{cne_darkblue}{HTML}{002060}
\definecolor{cne_yellow}{HTML}{FCD00F}
\definecolor{cne_medblue}{HTML}{2A427C}
\newcommand\cneopacity{0.5}

% Quelques raccourcis
\newcommand\cne{Centrale Nantes Études}
\newcommand\CNE{CENTRALE NANTES ÉTUDES}
\newcommand\jeh{Jour(s)-Étude Homme (JEH)}
\newcommand\link[1]{\textcolor{cne_blue}{\underline{\smash{#1}}}}

% Variables à modifier par les docs
\newcommand\varlegal{Tous droits de reproduction réservés}
\newcommand\varref{Reference}
\newcommand\varclient{Client}
\newcommand\vardoc{Document}
\newcommand\varversion{n°1}
\newcommand\varcovernumbering{\thepage/\pageref{LastPage}}
\newcommand\varcommonfooter{\vardoc\ Version \varversion\ \varref\ entre \CNE\ et \MakeUppercase{\varclient}}

% Aide aux variables
\newcommand\cnenocovernumbering{\renewcommand\varcovernumbering{}} % pas de numéro de page au début

% Affiche un logo J.E. pour un paragraphe
\newcommand\wrapjelogo{%
	\begin{wrapfigure}[4]{L}{62pt}
	\vspace{-\intextsep}
	\includegraphics[width=62pt]{je.pdf}
	\end{wrapfigure}
}

% Tailles
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\setlength{\parskip}{\medskipamount}
\setlength{\footskip}{1.5cm}
\setlength{\headsep}{1.5cm}
\setlength{\textheight}{\dimexpr(\textheight - 1cm)\relax}

% Sections classiques
\sectionfont{\color{cne_blue}}
\subsectionfont{\color{cne_darkblue}}

% Page de titre
\renewenvironment{titlepage}{%
	\clearpage\pagestyle{cnemain}%
}{%
	\clearpage%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Articles et alinéas (pour textes légaux)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\titleclass{\cnearticle}[1]{straight}
\newcounter{cnearticle}
\renewcommand{\thecnearticle}{Article \Roman{cnearticle}}
\titleformat{\cnearticle}[hang]{\color{cne_blue}\Large\bfseries}{}{0em}{\thecnearticle.~}
\titlespacing*{\cnearticle}{0pt}{3.5ex plus 1ex minus .2ex}{2.3ex plus .2ex} % copié de section
\newcommand{\cnearticleautorefname}{cnearticle}

\titlecontents{cnearticle}
[0pt]%
{\bfseries\makebox[6em][l]{\thecontentslabel.}}%
{}%
{\contentsmargin{0pt}}%
{\small\titlerule*[.5pc]{.}\contentspage}%
[\vspace{-\the\parskip}\smallskip]

\titleclass{\cnealinea}{straight}[\cnearticle]
\newcounter{cnealinea}[cnearticle]
\renewcommand{\thecnealinea}{Alinéa \Roman{cnealinea}}
\titleformat{\cnealinea}{\color{cne_darkblue}\large\bfseries}{}{0em}{\thecnealinea.~}
\titlespacing*{\cnealinea}{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex} % copié de subsection
\newcommand{\cnealineaautorefname}{cnealinea}

\titlecontents{cnealinea}
[1em]%
{\makebox[5em][l]{\small\thecontentslabel.}}%
{\small}%
{\contentsmargin{0pt}}%
{\small\titlerule*[.5pc]{.}\contentspage}%
[\vspace{-\the\parskip}\smallskip]


% Section non numérotée, TOC custom

\titleclass{\cnesection}[1]{straight}
\renewcommand{\thecnesection}{}
\titleformat{\cnesection}[hang]{\color{cne_blue}\Large\bfseries}{}{0em}{}
\titlespacing*{\cnesection}{0pt}{3.5ex plus 1ex minus .2ex}{2.3ex plus .2ex} % copié de section

\titlecontents{cnesection}
[0pt]%
{\bfseries}%
{}%
{\contentsmargin{0pt}}%
{\small\titlerule*[.5pc]{.}\contentspage}%
[\vspace{-\the\parskip}\smallskip]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Phases
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Fausse small caps (source: https://tex.stackexchange.com/a/55733)
\newlength\fake@f
\newlength\fake@c
\def\fakesc#1{%
  \begingroup%
  \xdef\fake@name{\csname\curr@fontshape/\f@size\endcsname}%
  \fontsize{\fontdimen8\fake@name}{\baselineskip}\selectfont%
  \uppercase{#1}%
  \endgroup%
}

\newcounter{phase}[section]
\newenvironment{phase}[2]{%
	\newcommand{\temp}{#2} % sauve temporairement le tarif de la phase (#2)
	\refstepcounter{phase}
	\hspace{0.05\linewidth}
	\begin{minipage}[t]{0.9\linewidth}
	{\bfseries\underline{P\fakesc{hase \thephase\ :}} #1} % nom de la phase (#1)
	\begin{itemize}
	\renewcommand\labelitemi{$\circ$}
}{%
	\end{itemize}
	{\bfseries T\fakesc{arif : }} \temp
	\end{minipage}
	\vspace{1em}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Headers et footers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Initialise les boîtes des headers et footers
% Doit être appelé après modification de vardoc, varversion, etc... /!\
\newcommand\initcnefooters{%
	\newbox\titlebox% Boite pour le titre de document
	\savebox\titlebox{%
		\parbox[c]{12cm}{\centering%
			\fontsize{50}{60}\selectfont\color{cne_yellow}\bfseries\MakeUppercase{\vardoc}	
		}
	}
	\newbox\footerbox% Footer usuel
	\savebox\footerbox{%
		\parbox[c]{0.85\textwidth}{\centering%
			\small \textit{\varlegal}\\
			\smallskip
			\varcommonfooter
		}
	}
	\newbox\mainfooterbox% Footer première page
	\savebox\mainfooterbox{%
		\parbox[c]{0.85\textwidth}{\color{white}\centering%
			\small \textit{\varlegal}\\
			\smallskip
			\varcommonfooter\\
			\smallskip
			\textbf{\cne} Junior-Entreprise de l’École Centrale de Nantes\\
			Association régie par la loi de 1901\\
			Membre de la Confédération Nationale des Junior-Entreprises\\
			\scriptsize
			1, rue de la Noë, BP 92101, 44321 NANTES Cedex 03 -- Tél. : +33 (0)2 40 37 16 21\\
			E-mail : \link{cne@ec-nantes.fr} -- Site : \link{centralenantesetudes.fr} -- Siret 331 616 375 00019 -- APE 9499Z\\
			N° INTRACOMMUNAUTAIRE : FR65331616375
		}
	}
}

% Titre de document
\newcommand\cnetitle[1]{%
	\vspace*{.5\textheight}
	\begin{center}
	\vspace{-\the\ht\titlebox}
	\usebox\titlebox
	\vspace{1cm}
	\parbox[c]{15cm}{%
		\color{white}\centering\Huge\bfseries #1
	}
	\end{center}
}


% Première page
\fancypagestyle{cnemain}{%
	\lhead{\cnemain} % affichage du fond de page
	\chead{\color{white}Réf. : \varref}
	\rhead{}
	\lfoot{}
	\cfoot{%
		\vspace{-\the\footskip}
		\vspace{-\the\ht\mainfooterbox}
		\usebox{\mainfooterbox}
	}
	\rfoot{\varcovernumbering}
}


% Autres pages
\fancypagestyle{cnestyle}{
	\lhead{\cnebg}
	\chead{}
	\rhead{}
	\lfoot{}
	\cfoot{%
		\vspace{-\the\ht\footerbox}
		\usebox{\footerbox}
	}
	\rfoot{\thepage/\pageref{LastPage}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tableaux
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\arraystretch}{1.8}
\newcolumntype{$}{>{\global\let\currentrowstyle\relax}} % charge le style de ligne
\newcolumntype{^}{>{\currentrowstyle}} % copie le style de ligne
\newcolumntype{Y}{>{\centering\arraybackslash}X} % colonne centrée, taille auto

% Modifie le style de ligne (si $ + ^ utilisés)
% Utilisation:
% 	\begin{tabularx}{$c^X^c^c^c^c^c}
% 	\rowstyle{....}
\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}%
	#1\ignorespaces
}

% Styles de ligne
\newcommand\cneheadrow{\rowstyle{\leavevmode\color{cne_blue}\bfseries}} % ligne d'en tête
\newcommand\cnerow{\rowstyle{\leavevmode\color{cne_blue}}} % ligne normale
\newcommand\cneline{\arrayrulecolor{cne_blue}\hline} % séparateur

\newenvironment{cnetab}[1]{%
	\rowcolors{2}{white}{cne_lightblue} % alternance de couleurs automatique
	\tabularx{\textwidth}{#1}%
}{%
	\endtabularx
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonds de page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usetikzlibrary{calc}

% Fond première page
\newcommand\cnemain{%
	\noindent\begin{tikzpicture}[remember picture,overlay]
	\filldraw[fill=cne_medblue, draw=cne_yellow, line width=.5cm] ($(current page.north west) + (.25,-.25)) rectangle ($(current page.south east) + (-.25,.25));
	\node[inner sep=0pt, opacity=1, anchor=north] (logo) at ($(current page.north) + (0,-2)$) {\includegraphics[height=4cm]{white.pdf}};
	\foreach \i in {-2,...,2} {
		\draw[draw=cne_yellow, line width=1mm, line cap=round, dash pattern=on 0 off 3\pgflinewidth] ($(current page.west) + (0.6,3*.1*\i)$) to ($(current page.west) + (4,3*.1*\i)$);
		\draw[draw=cne_yellow, line width=1mm, line cap=round, dash pattern=on 0 off 3\pgflinewidth] ($(current page.east) + (-0.6,3*.1*\i)$) to ($(current page.east) + (-4,3*.1*\i)$);
	}
\end{tikzpicture}}

% Fond autres pages
\newcommand\cnebg{%
	\noindent\begin{tikzpicture}[remember picture,overlay]
	% \node[inner sep=0pt, opacity=1, anchor=north east] (logo) at ($(current page.north east)+(-0.6,-0.6)$) {\includegraphics[height=2cm]{cne.pdf}};
	% \path [fill=cne_blue, opacity=\cneopacity] (current page.north west) to ($(current page.north) + (-2,0)$) to ($(current page.north west) + (0,-2)$);
	% \path [fill=cne_blue, opacity=\cneopacity] (current page.north west) to ($(current page.north) + (-8,0)$) to ($(current page.north west) + (0,-5)$);
	% \filldraw[fill=white, draw=cne_yellow, line width=.5cm] ($(current page.north west) + (.25,-.25)) rectangle ($(current page.south east) + (-.25,.25));
\end{tikzpicture}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\pagestyle{cnestyle}