from jinja2 import Environment, meta
from subprocess import call, Popen
from os import path, makedirs
from distutils.dir_util import copy_tree
from math import ceil
import yaml
import argparse
from datetime import timedelta

import locale
locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

env = Environment(
	block_start_string = '%[',
	block_end_string = ']%',
	variable_start_string = '[[',
	variable_end_string = ']]',
	comment_start_string = '%#',
	comment_end_string = '#%',
	lstrip_blocks = True,
	trim_blocks = True
)

def filter(fun):
	env.filters[fun.__name__] = fun
	def decorated(*args, **kwargs):
		return fun(*args, **kwargs)
	return decorated

@filter
def euros(prix):
	return "{0:.2f} €".format(prix).replace('.', ',')

@filter
def jeh(nb):
	return "{0} Jour{1}-Étude Homme (JEH)".format(nb, "s" if nb > 1 else "")

@filter
def sprange(max):
	return " ".join(map(str, range(0, max+1)))

@filter
def decpart(nb, count = 2):
	return ceil(10**count*(nb % 1))

@filter
def intpart(nb):
	return int(nb)

@filter
def refdate(date):
	try:
		return date.strftime("%d%m%y")
	except AttributeError:
		return date

@filter
def prettydate(date, format = "%A %d %B %Y"):
	try:
		return date.strftime(format).lstrip("0").replace(" 0", " ")
	except AttributeError:
		return date

@filter
def sex(sex, a = 'Monsieur', b = 'Madame'):
	return a if sex.strip().lower() == 'm' else b 

@filter
def init(str):
	return str[0].upper()

@filter
def delta(date, **kwargs):
	return date + timedelta(**kwargs)

@filter
def friday(date):
	return date + timedelta(days = 4 - date.weekday())

def prepare_template(name):
	deps = [name]
	with open(path.join('templates', name, 'template.yml'), 'r') as file:
		meta = yaml.load(file)
		deps.extend(meta.get('depends', []))
	for dep in deps:
		resources = path.join('templates', dep, 'resources')
		if path.exists(resources):
			copy_tree(resources, 'dist')
	return (path.join('templates', name, meta.get('tex', name + '.tmpl.tex')), meta)


def update_etude(doc):
	etude = doc['etude']
	phases = doc['etude']['phases']
	etude['duree_semaine'] = max([phase['semaine_fin'] for phase in phases])
	etude['prix'] = sum([phase['prix_jeh']*phase['nombre_jeh'] + phase['frais'] for phase in phases])
	etude['frais'] = sum([phase['frais'] for phase in phases])
	etude['nombre_jeh'] = sum([phase['nombre_jeh'] for phase in phases])

def main(args):

	if not path.exists('dist'):
		makedirs('dist')

	if args.resources:
		copy_tree(args.resources, 'dist')

	with open(args.source, 'r') as file:
		sub = yaml.load(file)
		update_etude(sub)

	template, meta = prepare_template(args.template)
	with open(template, 'r') as file:
		content = ''.join(file.readlines())
		tmpl = env.from_string(content)
		# print(meta.find_undeclared_variables(env.parse(content)))
		# print([(t, c) for (n, t, c) in env.lex(content) if t is not 'data'])
		# parse_schema(env.lex(content))

	name = args.name if args.name else env.from_string(meta.get('name', '[[etude.nom]]_' + args.template)).render(sub).strip()
	finalpath = path.join('dist', name)
	with open(finalpath + '.tex', 'w+') as file:
		res = tmpl.render(sub)
		file.write(res)

	p = Popen(['latexmk', '-f', '-xelatex', '-g', '--shell-escape', '-quiet', name  + '.tex'], cwd = 'dist')
	p.wait()
	if not args.keep: 
		p = Popen(['latexmk', '-c', name + '.tex'], cwd = 'dist')
		p.wait()

parser = argparse.ArgumentParser(description="Génération de documents")
parser.add_argument('source', metavar='source', type=str, help="fichier YML source")
parser.add_argument('template', metavar='template', type=str, help="document type à utiliser")
parser.add_argument('--keep', '-k', action='store_true', help="garder les fichiers auxiliaires")
parser.add_argument('--resources', '-r', type=str, metavar='folder', help="dossier de ressources à utiliser avant la compilation")
parser.add_argument('--name', '-n', type=str, metavar='name', help="nom du fichier final")

args = parser.parse_args()
main(args)